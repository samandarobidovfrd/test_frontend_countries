import './header.css'
import axios from "axios"
import {useEffect,useState} from "react"
function Header() {
    const [header, setHeader] = useState([])
    
    useEffect(() => {
        axios.get('http://164.92.234.214:8089/api/header').then(resp => {
            setHeader(resp.data.data)
            console.log(resp.data.data)
        }).catch(err => {
            setHeader([])
        })
    },[])
    return (
        <div className="countries-main-wrapper">
           {
                header.map(item => (
                    <h1>{item.title}</h1>
                ))
           }
        </div>
    )
}


export default Header
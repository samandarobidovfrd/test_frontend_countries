import './home.css'
import axios from "axios"
import {useEffect,useState} from "react"

function Home() {
    const [countries,setCountries]= useState([])

    useEffect(() => {
        axios.get('http://164.92.234.214:8089/api/countries').then(resp => {
            setCountries(resp.data.data)
            console.log(resp.data.data)
        }).catch(err => {
            console.log("error",err)
        })
    },[])
    return (
        <div className='home-main-wrapper'>
            {
                countries.map(item => (
                    <div key={item._id} className='home-inner-wrapper'>
                    <div className='country-flag-wrapper'>
                        <img alt='newimg' src={ 'https://flagcdn.com/w320/'+item.flag}></img>
                    </div>
                    <div className='country-info-wrapper'>
                        <div className='information'>
                                <p className='name'><b>Name: </b>{item.name}</p>
                                <p className='population'><b>Population: </b>{item.population}</p>
                                <p className='capital'><b>Capital: </b>{item.capital}</p>
                                <p className='region'><b>Region: </b>{item.region}</p>
                                <p className='area'><b>Area: </b>{item.area}</p>
                                <p className='currencies'><b>Currencies: </b>{item.currencies}</p>
                                <p className='independent'><b>Independent: </b>{item.independent ? 'yes':'no'}</p>
                                <p className='landlocked'><b>Landlocked: </b>{item.landlocked ? 'yes':'no'}</p>
                        </div>
                    </div>
                </div>
                ))
            }
        </div>
    )
}

export default Home